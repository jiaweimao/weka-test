/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.examples;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.OneR;
import weka.classifiers.rules.PART;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.J48;
import weka.core.Instances;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class WekaTest
{
    public static BufferedReader readDataFile(String filename)
    {
        BufferedReader inputReader = null;

        try {
            inputReader = new BufferedReader(new FileReader(WekaTest.class.getClassLoader().getResource(filename).getPath()));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }

        return inputReader;
    }

    public static Evaluation simpleClassify(Classifier model, Instances trainingSet, Instances testingSet) throws Exception
    {
        Evaluation validation = new Evaluation(trainingSet);

        model.buildClassifier(trainingSet);
        validation.evaluateModel(model, testingSet);

        return validation;
    }

    public static double calculateAccuracy(List<Prediction> predictions)
    {
        double correct = 0;
        for (Prediction np : predictions) {
            if (np.predicted() == np.actual()) {
                correct++;
            }
        }

        return 100 * correct / predictions.size();
    }

    public static Instances[][] crossValidationSplit(Instances data, int numberOfFolds)
    {
        Instances[][] split = new Instances[2][numberOfFolds];

        for (int i = 0; i < numberOfFolds; i++) {
            split[0][i] = data.trainCV(numberOfFolds, i);
            split[1][i] = data.testCV(numberOfFolds, i);
        }

        return split;
    }

    public static void main(String[] args) throws Exception
    {
        // I've commented the code as best I can, at the moment.
        // Comments are denoted by "//" at the beginning of the line.

        BufferedReader datafile = readDataFile("iris.arff");

        Instances data = new Instances(datafile);
        data.setClassIndex(data.numAttributes() - 1);

        // Choose a type of validation split
        Instances[][] split = crossValidationSplit(data, 10);

        // Separate split into training and testing arrays
        Instances[] trainingSplits = split[0];
        Instances[] testingSplits = split[1];

        // Choose a set of classifiers
        Classifier[] models = {new J48(),
                new PART(),
                new DecisionTable(),
                new OneR(),
                new DecisionStump()};

        // Run for each classifier model
        for (Classifier model : models) {
            // Collect every group of predictions for current model in a FastVector
            List<Prediction> predictions = new ArrayList<>();
            // For each training-testing split pair, train and test the classifier
            for (int i = 0; i < trainingSplits.length; i++) {
                Evaluation validation = simpleClassify(model, trainingSplits[i], testingSplits[i]);
                predictions.addAll(validation.predictions());

                // Uncomment to see the summary for each training-testing pair.
                // System.out.println(models[j].toString());
            }

            // Calculate overall accuracy of current classifier on all splits
            double accuracy = calculateAccuracy(predictions);

            // Print current classifier's name and accuracy in a complicated, but nice-looking way.
            System.out.println(model.getClass().getSimpleName() + ": " + String.format("%.2f%%", accuracy) + "\n=====================");
        }

    }
}