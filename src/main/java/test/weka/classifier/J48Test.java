/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.classifier;

import org.junit.jupiter.api.Test;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Sep 2018, 9:55 PM
 */
public class J48Test
{

    @Test
    public void test() throws Exception
    {
        J48 classifier = new J48();
        classifier.setConfidenceFactor(0.25f);
        classifier.setMinNumObj(2);
        classifier.setNumFolds(10);
        classifier.setReducedErrorPruning(true);

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(J48Test.class.getClassLoader().getResourceAsStream("glass.arff"));
        Instances dataSet = source.getDataSet();
        dataSet.setClassIndex(dataSet.numAttributes() - 1); // 设置分类属性所在行号

        classifier.buildClassifier(dataSet);
    }
}
