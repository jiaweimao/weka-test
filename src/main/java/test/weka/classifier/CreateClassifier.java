/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.classifier;

import org.junit.jupiter.api.Test;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Sep 2018, 8:02 PM
 */
public class CreateClassifier
{

    public Instances loadDataset(String path)
    {
        Instances dataset = null;
        try {
            dataset = ConverterUtils.DataSource.read(path);
            if (dataset.classIndex() == -1)
                dataset.setClassIndex(dataset.numAttributes() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataset;
    }

    public Classifier buildClassifier(Instances trainDataset)
    {
        MultilayerPerceptron m = new MultilayerPerceptron();
        try {
            m.buildClassifier(trainDataset);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return m;
    }

    public String evaluateModel(Classifier model, Instances traindataset, Instances testdataset)
    {
        Evaluation eval = null;
        try {
            // Evaluate classifier with test dataset
            eval = new Evaluation(traindataset);
            eval.evaluateModel(model, testdataset);
        } catch (Exception ex) {
        }
        return eval.toSummaryString("", true);
    }

    public void saveModel(Classifier model, String modelpath)
    {
        try {
            SerializationHelper.write(modelpath, model);
        } catch (Exception ex) {
        }
    }

    @Test
    void createClassifier()
    {
        Attribute petallength = new Attribute("petallength");
        Attribute petalwidth = new Attribute("petalwidth");
        ArrayList<Attribute> attributes = new ArrayList<>();
        ArrayList<String> classVals = new ArrayList<>();
        classVals.add("Iris-setosa");
        classVals.add("Iris-versicolor");
        classVals.add("Iris-virginica");

        attributes.add(petallength);
        attributes.add(petalwidth);

        attributes.add(new Attribute("class", classVals));
        Instances dataRaw = new Instances("TestInstances", attributes, 0);
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);

        Instances dataset = loadDataset(getClass().getClassLoader().getResource("iris.2D.arff").getPath());
        Filter filter = new Normalize();

        // divide dataset to train dataset 80% and test dataset 20%
        int trainSize = (int) Math.round(dataset.numInstances() * 0.8);
        int testSize = dataset.numInstances() - trainSize;


    }

    private Instances createInstance(Instances instances, double petallength, double petalwidth, double result)
    {
        instances.clear();
        double[] instanceValue1 = new double[]{petallength, petalwidth, 0};
        instances.add(new DenseInstance(1.0, instanceValue1));
        return instances;
    }

    private String classify(Instances instances, String path, List<String> classVals)
    {
        String result = "Not classified!";
        Classifier classifier = null;
        try {
            classifier = (MultilayerPerceptron) SerializationHelper.read(path);
            result = classVals.get((int) classifier.classifyInstance(instances.firstInstance()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
