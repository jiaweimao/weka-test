/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.core;


import org.junit.jupiter.api.Test;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * There are currently five different types of attributes available in WEKA:
 * <ul>
 * <li> numeric – continuous variables</li>
 * <li> date – date variables</li>
 * <li> nominal – predefined labels</li>
 * <li> string – textual data</li>
 * <li> relational – contains other relations, e.g., the bags in case of multi-instance data</li>
 * </ul>
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 20 Sep 2018, 8:17 PM
 */
public class InstancesTest
{

    @Test
    public void testIn() throws Exception
    {
        Instances dataset = ConverterUtils.DataSource.read(InstancesTest.class.getClassLoader().getResourceAsStream("iris.2D.arff"));
        assertEquals(dataset.classIndex(), -1);
    }

    @Test
    public void createInstance()
    {

    }

    @Test
    public void createAttributes() throws ParseException
    {
        // attributes
        ArrayList<Attribute> atts = new ArrayList<>();
        atts.add(new Attribute("att1")); // -numeric

        ArrayList<String> attVals = new ArrayList<>(); // -nominal
        int i;
        for (i = 0; i < 5; i++)
            attVals.add("val" + (i + 1));
        atts.add(new Attribute("att2", attVals));

        // - string
        atts.add(new Attribute("att3", (ArrayList<String>) null));

        // - relational
        ArrayList<Attribute> attsRel = new ArrayList<>();
        // -- numeric
        attsRel.add(new Attribute("att5.1"));
        // -- nominal
        ArrayList<String> attValsRel = new ArrayList<>();
        for (i = 0; i < 5; i++)
            attValsRel.add("val5." + (i + 1));
        attsRel.add(new Attribute("att5.2", attValsRel));

        // - date
        atts.add(new Attribute("att4", "yyyy-MM-dd"));

        Instances data;
        Instances dataRel = new Instances("att5", attsRel, 0);
        atts.add(new Attribute("att5", dataRel, 0));


        double[] vals;
        double[] valsRel;


        // 2. create Instances object
        data = new Instances("MyRelation", atts, 0);

        // 3. fill with data
        // first instance
        vals = new double[data.numAttributes()];
        // - numeric
        vals[0] = Math.PI;
        // - nominal
        vals[1] = attVals.indexOf("val3");
        // - string
        vals[2] = data.attribute(2).addStringValue("This is a string!");
        // - date
        vals[3] = data.attribute(3).parseDate("2001-11-09");
        // - relational
        dataRel = new Instances(data.attribute(4).relation(), 0);
        // -- first instance
        valsRel = new double[2];
        valsRel[0] = Math.PI + 1;
        valsRel[1] = attValsRel.indexOf("val5.3");
        dataRel.add(new DenseInstance(1.0, valsRel));
        // -- second instance
        valsRel = new double[2];
        valsRel[0] = Math.PI + 2;
        valsRel[1] = attValsRel.indexOf("val5.2");
        dataRel.add(new DenseInstance(1.0, valsRel));
        vals[4] = data.attribute(4).addRelation(dataRel);
        // add
        data.add(new DenseInstance(1.0, vals));

        // second instance
        vals = new double[data.numAttributes()];  // important: needs NEW array!
        // - numeric
        vals[0] = Math.E;
        // - nominal
        vals[1] = attVals.indexOf("val1");
        // - string
        vals[2] = data.attribute(2).addStringValue("And another one!");
        // - date
        vals[3] = data.attribute(3).parseDate("2000-12-01");
        // - relational
        dataRel = new Instances(data.attribute(4).relation(), 0);
        // -- first instance
        valsRel = new double[2];
        valsRel[0] = Math.E + 1;
        valsRel[1] = attValsRel.indexOf("val5.4");
        dataRel.add(new DenseInstance(1.0, valsRel));
        // -- second instance
        valsRel = new double[2];
        valsRel[0] = Math.E + 2;
        valsRel[1] = attValsRel.indexOf("val5.1");
        dataRel.add(new DenseInstance(1.0, valsRel));
        vals[4] = data.attribute(4).addRelation(dataRel);
        // add
        data.add(new DenseInstance(1.0, vals));

        // 4. output data
        System.out.println(data);
    }

    /**
     * 创建时，提供属性名称
     */
    @Test
    public void testNumeric()
    {
        Attribute numeric = new Attribute("name_of_attr");
    }

    /**
     * 日期内部以数值表示，但是为了方便日期的解析和输出，需要提供日期格式
     */
    @Test
    public void testDate()
    {
        Attribute date = new Attribute("name_of_attr", "yyyy-MM-dd");
    }

    @Test
    public void testNominal()
    {
        List<String> labels = new ArrayList<>();
        labels.add("label_a");
        labels.add("label_b");
        labels.add("label_c");
        labels.add("label_d");
        Attribute nominal = new Attribute("name_of_attr", labels);
    }

    /**
     * 字符串，如文本分类，构造函数和 nominal 一样
     */
    @Test
    public void testString()
    {
        Attribute string = new Attribute("name_of_attr", (List<String>) null);
    }

    /**
     * 创建一个 relational attribute that contains a relation with two attributes, a numeric and
     * a nominal attribute.
     */
    @Test
    public void testRelational()
    {
        ArrayList<Attribute> atts = new ArrayList<>();

        atts.add(new Attribute("rel.num"));

        List<String> values = new ArrayList<>();
        values.add("val_A");
        values.add("val_B");
        values.add("val_C");
        atts.add(new Attribute("rel.nom", values));

        Instances rel_struct = new Instances("rel", atts, 0);
        Attribute relational = new Attribute("name_of_attr", rel_struct);
    }

    /**
     * Generates the Instances object and outputs it in ARFF format to stdout.
     */
    @Test
    public void test()
    {
        // 1. set up attributes
        ArrayList<Attribute> atts = new ArrayList<>();
        // - numeric
        atts.add(new Attribute("att1"));

        // nominal
        ArrayList<String> attVals = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            attVals.add("val" + (i + 1));
        atts.add(new Attribute("att2", attVals));

        // string
        atts.add(new Attribute("att3", (ArrayList<String>) null));

        // date
        atts.add(new Attribute("att4", "yyyy-MM-dd"));

        // relational
        ArrayList<Attribute> attsRel = new ArrayList<>();
        // numeric
        attsRel.add(new Attribute("att5.1"));
        // nominal
        ArrayList<String> attValsRel = new ArrayList<>();
        for (int i = 0; i < 5; i++)
            attValsRel.add("val5." + (i + 1));
        attsRel.add(new Attribute("att5.2", attValsRel));
        Instances dataRel = new Instances("att5", attsRel, 0);
        atts.add(new Attribute("att5", dataRel, 0));

        // 2. create instances object
        Instances data = new Instances("MyRelation", atts, 0);

        // 3. fill with data
        // first instance
        double[] vals = new double[data.numAttributes()];
        vals[0] = Math.PI;
        vals[1] = attVals.indexOf("val3");
        vals[2] = data.attribute(2).addStringValue("This is a string!");

    }

    /**
     * @param args ignored
     * @throws Exception if generation of instances fails
     */
    public static void main(String[] args) throws Exception
    {
//        ArrayList<Attribute> attsRel;
//        ArrayList<String> attVals;
//        ArrayList<String> attValsRel;
//        Instances data;
//        Instances dataRel;
//        double[] vals;
//        double[] valsRel;
//        int i;
//
//        // 2. create Instances object
//        data = new Instances("MyRelation", atts, 0);
//
//        // 3. fill with data
//        // first instance
//        vals = new double[data.numAttributes()];
//        // - numeric
//        vals[0] = Math.PI;
//        // - nominal
//        vals[1] = attVals.indexOf("val3");
//        // - string
//        vals[2] = data.attribute(2).addStringValue("This is a string!");
//        // - date
//        vals[3] = data.attribute(3).parseDate("2001-11-09");
//        // - relational
//        dataRel = new Instances(data.attribute(4).relation(), 0);
//        // -- first instance
//        valsRel = new double[2];
//        valsRel[0] = Math.PI + 1;
//        valsRel[1] = attValsRel.indexOf("val5.3");
//        dataRel.add(new DenseInstance(1.0, valsRel));
//        // -- second instance
//        valsRel = new double[2];
//        valsRel[0] = Math.PI + 2;
//        valsRel[1] = attValsRel.indexOf("val5.2");
//        dataRel.add(new DenseInstance(1.0, valsRel));
//        vals[4] = data.attribute(4).addRelation(dataRel);
//        // add
//        data.add(new DenseInstance(1.0, vals));
//
//        // second instance
//        vals = new double[data.numAttributes()];  // important: needs NEW array!
//        // - numeric
//        vals[0] = Math.E;
//        // - nominal
//        vals[1] = attVals.indexOf("val1");
//        // - string
//        vals[2] = data.attribute(2).addStringValue("And another one!");
//        // - date
//        vals[3] = data.attribute(3).parseDate("2000-12-01");
//        // - relational
//        dataRel = new Instances(data.attribute(4).relation(), 0);
//        // -- first instance
//        valsRel = new double[2];
//        valsRel[0] = Math.E + 1;
//        valsRel[1] = attValsRel.indexOf("val5.4");
//        dataRel.add(new DenseInstance(1.0, valsRel));
//        // -- second instance
//        valsRel = new double[2];
//        valsRel[0] = Math.E + 2;
//        valsRel[1] = attValsRel.indexOf("val5.1");
//        dataRel.add(new DenseInstance(1.0, valsRel));
//        vals[4] = data.attribute(4).addRelation(dataRel);
//        // add
//        data.add(new DenseInstance(1.0, vals));
//
//        // 4. output data
//        System.out.println(data);
    }
}
