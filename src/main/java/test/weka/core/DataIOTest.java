/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.core;

import org.junit.jupiter.api.Test;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.datagenerators.classifiers.classification.RDG1;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Nov 2018, 9:44 AM
 */
public class DataIOTest
{
    @Test
    public void testLoad() throws Exception
    {
        Instances instances = ConverterUtils.DataSource.read(getClass().getClassLoader().getResourceAsStream("iris.2D.arff"));
        assertEquals(instances.numAttributes(), 3);

        Attribute attribute = instances.attribute(0);
        assertEquals(attribute.type(), Attribute.NUMERIC);
        assertEquals(instances.attribute(1).type(), Attribute.NUMERIC);
        assertEquals(instances.attribute(2).type(), Attribute.NOMINAL);
    }

    @Test
    public void testWrite()
    {
        RDG1 generator = new RDG1();
        generator.setMaxRuleSize(5);
    }
}
