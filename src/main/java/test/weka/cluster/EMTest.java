/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 JiaweiMao
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in the
 * Software without restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS ORCOPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OROTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package test.weka.cluster;

import org.junit.jupiter.api.Test;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.EM;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Nov 2018, 1:55 PM
 */
public class EMTest
{
    @Test
    public void test() throws Exception
    {
        Instances data = ConverterUtils.DataSource.read("");
        EM clusterer = new EM();
        clusterer.setMaxIterations(100);
        clusterer.buildClusterer(data);

        ClusterEvaluation evaluation = new ClusterEvaluation();
        evaluation.setClusterer(clusterer);
        evaluation.evaluateClusterer(data);

        double[] clusterAssignments = evaluation.getClusterAssignments();

    }


}
